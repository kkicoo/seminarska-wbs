/**
 * Created by Kkicoo7 on 26-Aug-15.
 */


var app = angular.module('movieApp', []);


app.controller('movieCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.searchMovie = function (search) {
        var query = "PREFIX dbo:" + "<http://dbpedia.org/property/>" + "PREFIX dbpedia:" + "<http://dbpedia.org/resource/>" +
            "SELECT DISTINCT ?film WHERE { ?film dbo:starring dbpedia:" + $scope.search1 +
            ($scope.search2 ? ";  dbo:starring dbpedia:" + $scope.search2+".}" : ".}");

        $('#search').attr('disabled',"");
        $('#search').attr('value',"Searching...");
        $http.post('http://localhost:3000/', {query: query}).
            then(function (response) {

                $scope.films = response.data;
                document.getElementById("search").disabled = false;
                $('#search').attr('value',"Search");

            }, function (response) {

                // called asynchronously if an error occurs

                // or server returns response with an error status.
            });
    };
    $scope.openModal=function(filmName,starring){
        $scope.name=filmName;
        $scope.actors=starring;
        $scope.names=[];
        for(var i=0;i<starring.length;++i){
            var temp=starring[i].actor.split('/');
            $scope.names.push(temp[temp.length-1]);

            }
        $('#modal').modal();
    }


}]);
